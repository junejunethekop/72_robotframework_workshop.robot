***Settings***
Library  Builtin

***Variables***
${pie}=  3.14
${r}=  5
${area}=  0
    

***Test Cases***
1. คำนวณพื้นที่วงกลม
    ${area}=  SquareArea  ${pie}  ${r}
    Log To Console  \nArea: ${area}


***Keywords***
squareArea
    [Arguments]  ${paraPie}  ${paraR}
    ${respArea}=  Evaluate  ${paraPie} * ${paraR} * ${paraR}
    [Return]  ${respArea}
